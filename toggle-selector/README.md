# Toggle Selector

## Functionality
Creates a switch that lets you toggle between two or more options.
It is based on this animated radio button code: http://codepen.io/chrisN/pen/XXBpKw

[Source](toggle-selector.tag.html) / [Example](toggle-selector.demo.html)

## Requirements
`<toggle-selector>` requires the following scripts to be loaded:

- [Underscore.js](http://www.underscorejs.org)
- [events.mixin.js](../mixin/events.mixin.js)

## Usage
To use a `<toggle-selector>`, you pass it the options to toggle between, and optionally which one is preselected. You then listen for the `select` event either on the tag using `onselect` or on the DOM. You can select another option by changing the `selected` attribute.

## Attributes
A `<toggle-selector>` has the following attributes:

| attribute | type | description |
| --- | --- | --- |
| `options` | Array | array of option objects |
| `selected` | String *optional* | the value of the selected option. If not set, the first option is selected |

Structure of the option object:

| attribute | type | description |
| --- | --- | --- |
| `value` | String | key of the option |

You can add any other properties to your option objects, which you can then use in the yield.

## Yield
You specify how you want to present each option by filling the `<toggle-selector>...</toggle-selector>`. 
The following attributes are available to you in the yield:

| attribute | type | description |
| --- | --- | --- |
| `option` | Object | the current option object to be filled in. |
| `index` | number | the number of the option, starting at 0 |

## Styles
The style classes shown (even, sorted, descending) are switched on and off based on the state of the table:

| style class | attached to | description |
| --- | --- | --- |
| `.sorted` | table header | the table is sorted by that header. only one header will have .sorted |
| `.descending` | table header | means the table is sorted in descending order. otherwise it is sorted ascending.|
| `.even` | table rows | added only if the row or column is an even number. so the 2nd, 4th, etc row or col |

## Result
The tag renders itself as a table with the following structure:

	<toggle-selector>
		<input type="radio" name="toggle_option" id="toggle-0">
		...(more inputs)...
		<label for="toggle-0"><p>English speaker</p></label>
		...(a label for each input)...
		<div class="toggle_option_slider"></div>
	</toggle-selector>

The inputs are hidden. Clicking the labels selects the hidden inputs. The slider is animated in front of the selected option using CSS.
