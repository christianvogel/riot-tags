/**
 * A Riot Mixin that adds generic event listening and publishing to tags.
 * <p>
 * Recommended usage: <pre>riot.mixin(Mixins.Events);</pre>
 */

if(!Mixins) var Mixins = {}

Mixins.Events = {

    /**
     * Registers event handlers for on[option] properties. This is necessary because
     * RIOT on... handlers only catch normal events and not custom events!
     */
    init: function() {
        var tag = this
        tag.on('before-mount', function() {
            for(var option in tag.opts) {
                if(option.startsWith('on')) {
                    var eventName = option.substring(2)
                    var value = tag.opts[option]
                    if(value instanceof Function) {
                        tag.onEvent(eventName, value.bind(tag))
                    } else {
                        tag.onEvent(eventName, tag.dispatchEvent.bind(tag, value))
                    }
                }
            }
        })

    },

    /**
     * Listens for DOM events (not RIOT observables) with the given name on the current tag and all of its children.
     * Just like with RIOT observable handlers, once caught events will not bubble up.
     * @param eventName string: name of the event to listen for
     * @param handler function: to be called with the event. the event will be cancelled unless the function returns truthy
     */
    onEvent: function(eventName, handler) {
        function eventHandler(e) {
            var bubble = handler(e)
            if(!bubble) e.stopPropagation()
        }
        this.root.addEventListener(eventName, eventHandler)
    },

    /**
     * Fire a custom DOM event on the root of this tag, which can bubble up the DOM.
     * @param name the event name
     * @param options the event options
     */
    dispatchEvent: function(name, data) {
        this.root.dispatchEvent(new CustomEvent(name, {
            bubbles: true,
            cancelable: true,
            detail: data
        }))
    }

}
