# Sortable Table

## Functionality

Creates a self-sorting table with a header. By default, the data is presented as-is. Each header is clickable to sort that column in ascending or descending order (by clicking again).

[Source](../tag/chartjs-chart.tag.html) / [Example](../test-chartjs-chart-tag.html)

## Requirements

`<sortable-table>` requires the following scripts to be loaded:

- [Underscore.js](http://www.underscorejs.org)

## Usage
To use a `<sortable-table>`, you create rows and columns and assign them to the tag. Inside of the tag you use `<yield to="…">` tags to specify how you want the headers and values to render. The table automatically sets classes for you to style.

## Attributes
| attribute | type | description |
| --- | --- | --- |
| `columns` | Array | array of column objects |
| `rows` | Array | array of rows, which are arrays of values |

All of the above options are actually match one-on-one the json configuration you would normally pass the ChartJS class.

## Functions
| function | description | parameters
| --- | --- | --- |
| `sortBy` | Sorts the table by column | `columntitle`: string, `descending`: boolean |

## Yields
In order to render a result, you need to specify the yields for `<sortable-table>`. [See the example to see it in action](../test-chartjs-chart-tag.html). The following yields must be passed:

| yield | description | available attributes |
| --- | --- | --- |
| `th` | table header cell | `column`: column, `column_index`: number |
| `td` | table value cell | `value`: any, `row`: row, `column`: column, `row_index`: number, `column_index`: number, |

## Styles
The style classes shown (even, sorted, descending) are switched on and off based on the state of the table:

| style class | attached to | description |
| --- | --- | --- |
| `.sorted` | table header | the table is sorted by that header. only one header will have .sorted |
| `.descending` | table header | means the table is sorted in descending order. otherwise it is sorted ascending.|
| `.even` | table rows | added only if the row or column is an even number. so the 2nd, 4th, etc row or col |

## Result
The tag renders itself as a table with the following structure:

	<sortable-table>
	    <thead>
	        <tr>
	            <th class="even sorted descending">
	                <yield from="header"/>
	            </th>
	            ...
	        </tr>
	        ...
	    </thead>
	    <tbody>
	        <tr class="even">
	            <td class="even">
	                <yield from="field"/>
	            </td>
	            ...
	        </tr>
	        ...
	    </tbody>
	</sortable-table>
