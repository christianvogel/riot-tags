# RIOT TAG LIBRARY

The Kimengi Riot tag library provides some basic reusable tags.
Currently supported tags are:

## Sortable Table

Lets you easily create dynamically updating sortable tables.

	<sortable-table columns="{cols}" rows="{rows}">
	    <yield to="header">{column.title}</yield>
	    <yield to="field">{value}</yield>
	</sortable-table>

[Source](sortable-table/sortable-table.tag.html) /
[Documentation](sortable-table/README.md) / 
[Example](sortable-table/sortable-table.demo.html)

## ChartJS Chart

Create Chart.JS charts easily using a thin Riot wrapper.

	<chartjs-chart type="line" data="{chartdata}"></chartjs-chart>
	
[Source](chartjs-chart/chartjs-chart.tag.html) /
[Documentation](chartjs-chart/README.md) / 
[Example](chartjs-chart/chartjs-chart.demo.html)

## Toggle Selector

Create a toggle between options.

	<toggle-selector options="{options}" onselect="{handler}"></toggle-selector>
	
[Source](toggle-selector/toggle-selector.tag.html) /
[Documentation](toggle-selector/README.md) / 
[Example](toggle-selector/toggle-selector.demo.html)

## Adhere to the Guidelines

[![RiotJS Style Guide badge](https://cdn.rawgit.com/voorhoede/riotjs-style-guide/master/riotjs-style-guide.svg)](https://github.com/voorhoede/riotjs-style-guide)

* [Module based development](https://github.com/voorhoede/riotjs-style-guide#module-based-development)
* [Tag module names](https://github.com/voorhoede/riotjs-style-guide#tag-module-names)
* [1 module = 1 directory](https://github.com/voorhoede/riotjs-style-guide#1-module--1-directory)
* [Use `*.tag.html` extension](https://github.com/voorhoede/riotjs-style-guide#use-taghtml-extension)
* [Use `<script>` inside tag](https://github.com/voorhoede/riotjs-style-guide#use-script-inside-tag)
* [Keep tag expressions simple](https://github.com/voorhoede/riotjs-style-guide#keep-tag-expressions-simple)
* [Keep tag options primitive](https://github.com/voorhoede/riotjs-style-guide#keep-tag-options-primitive)
* [Harness your tag options](https://github.com/voorhoede/riotjs-style-guide#harness-your-tag-options)
* [Assign `this` to `tag`](https://github.com/voorhoede/riotjs-style-guide#assign-this-to-tag)
* [Put tag properties and methods on top](https://github.com/voorhoede/riotjs-style-guide#put-tag-properties-and-methods-on-top)
* [Avoid fake ES6 syntax](https://github.com/voorhoede/riotjs-style-guide#avoid-fake-es6-syntax)
* [Avoid `tag.parent`](https://github.com/voorhoede/riotjs-style-guide#avoid-tagparent)
* [Use `each ... in` syntax](https://github.com/voorhoede/riotjs-style-guide#use-each--in-syntax)
* [Put styles in external files](https://github.com/voorhoede/riotjs-style-guide#put-styles-in-external-files)
* [Use tag name as style scope](https://github.com/voorhoede/riotjs-style-guide#use-tag-name-as-style-scope)
* [Document your tag API](https://github.com/voorhoede/riotjs-style-guide#document-your-tag-api)
* [Add a tag demo](https://github.com/voorhoede/riotjs-style-guide#add-a-tag-demo)
* [Lint your tag files](https://github.com/voorhoede/riotjs-style-guide#lint-your-tag-files)
* [Add badge to your project](https://github.com/voorhoede/riotjs-style-guide#add-badge-to-your-project)
